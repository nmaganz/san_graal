# My project's README

Git repository for the *Sangraal* project. 

## Dataset list

Downloaded Datasets:
'GSE27562', 'GSE16443', 'GSE36076', 'GSE49515', 'GSE30174', 'GSE15932', 'GSE47862', 'GSE39345', 'GSE47860', 'GSE51490', 'GSE20189', 'GSE47756', 'GSE58208', 'GSE47861'

Annotated Datasets:

Dataset:  Platform:   BoxPlot:   Keys:     Cases: (1)                                                   Controls: (0)                           Ignored:

GSE27562  GPL570      Ok         Ok        Malignant breast, ectopic (other cancers), Pre-Surgery       Normal                                  Benign, Post-Surgery
GSE16443  GPL2986     ???        ???       Breast cancer                                                Healthy, preg, breast feeding, mens.    None
GSE36076  GPL570      Ok         Ok        Gastric, HCC, pancreatic                                     Normal                                  None
GSE49515  IGNORE -- Same data as GSE36076
GSE30174  GPL570      Ok         Ok        Prostate                                                     Healthy                                 During/Post radiation therapy
GSE15932  GPL570      Ok         Ok        Pancreatinc, pancreatic + diabetes                           Healthy, diabetes only                  None             
GSE47862  GPL17279    Ok         BAD       All cancer, regardless fam/BRCA                              All non-cancer, regardless fam/BRCA     None
GSE47860  IGNORE -- Same data as GSE47862
GSE39345  GPL6104     ???        Ok        Lung cancer before chemo                                     Healthy                                 Lung after chemo
GSE51490  GPL15640    Ok         Ok        Renal cancer before vaccine                                  Healthy                                 Renal cancer after vaccine
GSE20189  GPL571      Ok         Ok        Lung Cancer Cases, Cases Excluded                            Controls                                Non-cases excluded
GSE47756  GPL10558    ???        ???       Colorectal metastatic, colorectal non-metastatic             Healthy                                 None
GSE58208  GPL570      Ok         Ok        Hepatocellular                                               Healthy controls, Chronic Hep B         None               
GSE47861  IGNORE -- Same data as GSE47862

E-MTAB-1532 --> processed and stored in E-MEXP-3756_processed.sdrf.txt, disease state saved in $Factor.Value.disease.state. .
E-MEXP-3756 --> processed and stored in E-MEXP-3756_processed.sdrf.txt, disease state saved in $Factor.Value.disease.state. . Pooled before/after colonoscopy together.

Updated Dataset Numbers:
Dataset:    Cases:    Controls:   Total:
GSE27562    79        31          110
GSE27562_br 57        31          88
GSE27562_ec 22        31          53
GSE16443    67        63          130
GSE36076    16        10          26
GSE36076_ga 3         10          13
GSE36076_he 10        10          20
GSE36076_pa 3         10          13
GSE30174    10        10          20
GSE15932    16        16          32
GSE47862    158       163         321
GSE39345    32        20          52
GSE51490    9         9           18
GSE20189    81        80          161
GSE47756    55        38          93
GSE58208    10        17          27
EMTAB1532   100       100         200
EMEXP3756   20        40          60


